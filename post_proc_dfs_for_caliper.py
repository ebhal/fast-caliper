from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os

import pandas as pd
import yaml

from chip_code.make_nonVBF_plots import get_primary_datasets
from fast_plotter.utils import decipher_filename

signal = [
    "ggF125",
    "ggZH_ZToQQ_125",
    "ttH125",
    "WminusH125",
    "WplusH125",
    "ZH125"
]


def write_caliper_config(sig_df_name, bkg_df_name, category, significance, sensitive_vars):
    config_dict = {
        "general": {
            "df_sig": sig_df_name,
            "df_bkg": bkg_df_name,
            "cut_on": [{var: ">"} for var in sensitive_vars],
            "ignore": ["category"],
            "name_content": "weight_nominal:sumw",
            "figure_of_merit": significance,
            "type": "cut",
            "out_df_name": os.path.join("output_dfs", "out_category_{}_{}.csv".format(category, significance)),
        }
    }
    out_config = os.path.join("config", "angular_var_category_{}_{}.yaml".format(category, significance))
    if not os.path.exists("config"):
        os.mkdir("config")
    with open(out_config, "w") as out_file:
        out_file.write(yaml.dump(config_dict))
    print("Saved a template fast-caliper config in", out_config)


def make_sig_bkg_dfs_for_process(sig_df, bkg_df, category, outdir):
    sig_df = sig_df.copy()
    bkg_df = bkg_df.copy()

    sig_df = sig_df[sig_df["category"] == category]
    bkg_df = bkg_df[bkg_df["category"] == category]

    common_out_name = os.path.join(outdir, "tbl_category_{}".format(category))
    sig_df_name = common_out_name + "_signal.csv"
    bkg_df_name = common_out_name + "_background.csv"
    sig_df.to_csv(sig_df_name, index=False)
    bkg_df.to_csv(bkg_df_name, index=False)
    print("Saved signal and background dfs in {}_{{signal,background}}.csv".format(common_out_name))
    return sig_df_name, bkg_df_name


def main(in_file, year, outdir="./input_dfs", significance="asimov_uncertainty5pc"):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    binning_dims = decipher_filename(in_file)[0]
    sensitive_vars = []
    for dim in binning_dims:
        if any(dim == d for d in ["dataset", "region", "category"]):
            continue
        sensitive_vars.append(dim)

    met_pds, photon_pds, ele_pds = get_primary_datasets(year)
    data = met_pds + photon_pds + ele_pds

    # Cleanup dataframe. Drop CRs and sidebands, drop data
    df = pd.read_csv(in_file)
    df = df[~df["dataset"].isin(data)]
    df = df[df["region"] == 0]
    df = df[df["category"] > 0.]
    cols_to_drop = ["region"]
    for col in df.columns:
        if "weight" in col and "weight_nominal" not in col:
            cols_to_drop.append(col)
    df.drop(columns=cols_to_drop, inplace=True)

    lumi = {
        2016: 35920,
        2017: 41530,
        2018: 59740
    }
    df["weight_nominal:sumw"] *= lumi[year]
    df["weight_nominal:sumw2"] *= lumi[year]**2

    sig_datasets = df[df["dataset"].isin(signal)].copy()
    bkg_datasets = df[~df["dataset"].isin(signal)].copy()

    for cat in df["category"].unique():
        sig_df_name, bkg_df_name = make_sig_bkg_dfs_for_process(sig_datasets, bkg_datasets, cat, outdir)
        write_caliper_config(sig_df_name, bkg_df_name, cat, significance, sensitive_vars)


if __name__ == "__main__":
    parser = ArgumentParser(description=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("dataframe", type=str, help="Dataframe")
    parser.add_argument("year", type=int, choices=[2016, 2017, 2018], help="Year of data taking so data is filtered out")
    parser.add_argument("--outdir", "-o", type=str, default="./input_dfs", help="Output directory for dataframes")
    parser.add_argument("--significance", "-s", type=str, choices=["asimov", "asimov_uncertainty5pc", "SoverSqrtB", "SoverSqrtB_uncertainty5pc"], default="asimov_uncertainty5pc", help="Figure of merit to use in fast-caliper (written to configs)")
    args = parser.parse_args()

    main(args.dataframe, args.year, outdir=args.outdir, significance=args.significance)
