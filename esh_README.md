# Making significance plots

Generate the dataframe containing the variable being studied. First, set some environment variables for re-used variables:

``bash
df="<dataframe>"  # full path to dataframe from fast-carpenter
year=<year>  # 2016, 2017, or 2018
split_dfs_dir="<dir>"  # path to store the dataframes of signal and background for each category
significance="<significance>"  # figure of merit to investigate in fast-caliper (SoverSqrtB/SoverSqrtB_uncertainty5pc/asimov/asimov_uncertainty5pc)

Post-process to separate signal and background, and each category:

```bash
python post_proc_dfs_for_caliper.py -o $split_dfs_dir -s $significance $df $year
```

Each category and figure of merit has its own config under `config/`. This is to avoid having to overwrite every config if you want to study another figure of merit. The variable that's cut on is guessed from the dataframe name, and assumed that you want to cut on ">" than that variable. These generated configs should be thought of as templates, so editing for the different scenarios you want to investigate is simpler and involves copying/editing less code. Once done, run `fast-caliper` over the dataframes created in the previous stage, e.g.:

```bash
fast_caliper config/angular_var_category_1.01_${significance}.yaml
```

To make this simpler, and to combine the output from all categories in a process, a bash script `run_caliper_all_categories_and_combine.sh` can be run, giving the process (ttH/VH/ggF/monojet) and figure of merit as arguments:

```bash
./run_caliper_all_categories_and_combine.sh ttH $significance
```

Then finally, run the script to plot the significances:

```bash
python plot_multi_signifs.py -y $year -p <process> <dataframes from caliper - individual for category or combined>
```

This is designed to plot multiple significances from a single process. So all the previous steps can be run for the different figures of merit, and the dataframes given as input to the plotting script. For example,

```bash
df="/my_dataframes/tbl_dataset.region.min_omega_tilde.category--min_omega_tilde.csv"
split_dfs_dir="input_dfs_min_omega_tilde/"
year=2017
process="ttH"

python post_proc_dfs_for_caliper.py -o $split_dfs_dir -s SoverSqrtB $df $year
python post_proc_dfs_for_caliper.py -o $split_dfs_dir -s SoverSqrtB_uncertainty5pc $df $year
python post_proc_dfs_for_caliper.py -o $split_dfs_dir -s asimov $df $year
python post_proc_dfs_for_caliper.py -o $split_dfs_dir -s asimov_uncertainty5pc $df $year

./run_caliper_all_categories_and_combine.sh $process SoverSqrtB
./run_caliper_all_categories_and_combine.sh $process SoverSqrtB_uncertainty5pc
./run_caliper_all_categories_and_combine.sh $process asimov
./run_caliper_all_categories_and_combine.sh $process asimov_uncertainty5pc

python plot_multi_signifs.py -y $year -p $process out_${process}_combined_SoverSqrtB.csv out_${process}_combined_SoverSqrtB_uncertainty5pc.csv out_${process}_combined_asimov.csv out_${process}_combined_asimov_uncertainty5pc.csv 
```
