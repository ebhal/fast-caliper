from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml

plt.rcParams["figure.figsize"] = [5, 6]
plt.rcParams["axes.formatter.use_mathtext"] = True


def main(in_files, processes, plotting_cfg):
    ax = plt.gca()

    possible_signifs = {
        "SoverSqrtB": r"$S/\sqrt{B}$",
        "SoverSqrtB_uncertainty5pc": r"$S/\sqrt{B}$ (5% bkg. syst.)",
        "asimov": "Asimov significance",
        "asimov_uncertainty5pc": "Asimov significance (5% bkg. syst.)",
    }

    # Add dataframe info to these lists to make sure the same significances and variables are used for all dfs
    signifs = []
    x_axes = []
    dfs = []

    for f in in_files:
        df = pd.read_csv(f)
        if "Unnamed: 0" in df.columns:
            df.drop(columns=["Unnamed: 0"], inplace=True)

        x_axis = df.columns[0]
        x_axes.append(x_axis)
        df = df[np.isfinite(df[x_axis])]  # drop infinities
        df.reset_index(inplace=True)
        dfs.append(df)

        # Get the column name that contains the significance
        signif = []
        for col in possible_signifs.keys():
            if col in df.columns:
                signif.append(col)
        if len(signif) > 1:
            print("Multiple significance columns found: {}. Will use {}".format(signif, signif[0]))
        signif = signif[0]
        signifs.append(signif)

    if len(list(set(signifs))) > 1:
        raise ValueError("Trying to aggregate different significances. Make sure each dataframe uses the same metric for significance")
    signif = signifs[0]
    if len(list(set(x_axes))) > 1:
        raise ValueError("Trying to aggregate different variables. Make sure each dataframe uses the same variable")
    x_axis = x_axes[0]

    # Get x-axis label from plotting config
    plt_cfg = yaml.safe_load(open(plotting_cfg, "r"))
    var_replacements = plt_cfg["bin_variable_replacements"]
    if x_axis in var_replacements.keys():
        x_label = var_replacements[x_axis]
    else:
        x_label = x_axis

    # Make scatter plot with connecting line for each dataframe
    for i, df in enumerate(dfs):
        df.plot.scatter(
            x=x_axes[i],
            y=signifs[i],
            ax=ax,
            color=plt.cm.tab10(i),
            label=processes[i]
        )
        df.plot.line(
            x=x_axes[i],
            y=signifs[i],
            label="",
            ax=ax,
            color=plt.cm.tab10(i)
        )
    ax.grid(True)
    ax.set_axisbelow(True)

    ax.set_xlim([dfs[0][x_axis].min(), dfs[0][x_axis].max()])  # manually set x-axis limits from dataframes
    ax.set_xlabel(x_label)
    ax.set_ylim([0., ax.get_ylim()[1]])  # set lower y-axis limit to zero
    ax.set_ylabel(possible_signifs[signif])
    ax.legend(loc="upper right")

    # ADD MORE ANNOTATIONS (LIKE THE TOP BANNER ON OUR OTHER PLOTS)

    out_file = "significance_plot_{}_{}_{}.pdf".format(x_axis, signif, "_".join(processes))
    plt.savefig(out_file, dpi=200)
    print("Saving output file:", out_file)


if __name__ == "__main__":
    parser = ArgumentParser(description=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("dataframes", type=str, nargs="+", help="Dataframe with significances")
    parser.add_argument("--processes", "-s", default=[], action="append", help="Signal process each dataframe corresponds to. Call once for each dataframe")
    parser.add_argument("--plotting_cfg", "-p", type=str, default=os.path.join("..", "analysis", "plotting_configs", "plot_config_all_years_prescaled.yml"), help="Plotting config to add bin variable replacements for the variable")
    args = parser.parse_args()

    if len(args.processes) != len(args.dataframes):
        raise IOError("You need to specify one process per dataframe")

    main(args.dataframes, args.processes, args.plotting_cfg)
