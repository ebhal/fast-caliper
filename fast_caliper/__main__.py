#from __future__ import print_function
import glob
from array import array
import os
import collections
import math
import pandas as pd
from fast_plotter.interval_from_str import interval_from_string
pd.options.display.max_columns = 20
"""
Optimizes selection cuts based on signal and background dataframes
"""

class Analysis:

    df_sig = ""
    df_bkg = ""
    cut_on = {}
    ignore = []
    name_content = ""
    figure_of_merit = ""
    out_df_name = ""

    def __init__(self, config_file):

        with open(config_file, "r") as infile:
            import yaml
            cfg = yaml.load(infile)
            self.df_sig = str(cfg["general"]["df_sig"])
            self.df_bkg = str(cfg["general"]["df_bkg"])
            self.cut_on = {}
            for i,ele in enumerate(cfg["general"]["cut_on"]):
                self.cut_on[list(ele.keys())[0]] = ele[list(ele.keys())[0]]
            self.ignore = cfg["general"]["ignore"]
            self.name_content = str(cfg["general"]["name_content"])
            self.figure_of_merit = str(cfg["general"]["figure_of_merit"])
            self.type = str(cfg["general"]["type"])
            self.out_df_name = str(cfg["general"]["out_df_name"])

        if not os.path.exists(os.path.dirname(self.out_df_name)):
            os.makedirs(os.path.dirname(self.out_df_name))


class DummyCollector():
    def collect(self, *args, **kwargs):
        pass

def cumsum_nd2(df, cut_on, ignore, content, type_optimization):  
    
    df = df.drop(columns=ignore, axis=1)
    ignore_local = [content]
    columns_list = [e for e in df.columns.values.tolist() if e not in ignore_local]
    df = df.groupby(columns_list).sum()

    cut_vars = list(df.index.names)
    dims = list(range(df.index.nlevels)) 

    series = []
    
    df = df.reset_index(list(cut_on.keys()))
    for index, row in df.iterrows():
        df_local = df
        for var in cut_vars:
            if cut_on[var] == ">":
                df_local = df_local[(df_local[var]>=row[var])]
            else:
                df_local = df_local[(df_local[var]<row[var])]
        sum0 = df_local[content].sum()
        series.append(sum0)

    df[content] = series

    return df

def cumsum_nd(df, cut_on, ignore, content, type_optimization):  
    
    df = df.drop(columns=ignore, axis=1)
    ignore_local = [content]
    columns_list = [e for e in df.columns.values.tolist() if e not in ignore_local]
    df = df.groupby(columns_list).sum()

    dims = list(range(df.index.nlevels)) 
        
    summed = df.reset_index(list(cut_on.keys()))
    list_vars = list(cut_on.keys())
    list_vars.append(content)
    summed = summed[list_vars]
    summed = summed.sort_values(by=list(cut_on.keys()))
    summed = summed.set_index(list(cut_on.keys()))

    for level in dims:
        group = list(dims[:])
        group.pop(level)

        if cut_on[df.index.names[level]] == ">":              
            summed = summed.iloc[::-1].groupby(level=group).cumsum()
            summed = summed.iloc[::-1]
        elif cut_on[df.index.names[level]] == "<":                        
            summed = summed.groupby(level=group).cumsum() 

    return summed

def get_values_from_bin_ranges(df, x_axis):
    intervals = interval_from_string(df[x_axis])
    df[x_axis] = intervals.left
    #df = df.sort_values(x_axis)
    return df


def process_args(args=None):
    from argparse import ArgumentParser
    parser = ArgumentParser(description=__doc__)
    parser.add_argument("caliper_cfg", type=str,
                        help="Config to run over")
    parser.add_argument('-c','--comb', action='append', help='comma separated list of files', required=False)
    return parser.parse_args()

def compute_significance_asimov(signal,background):
    result = 0
    if signal>0 and background>0:
        result = math.sqrt(2*((signal+background)*math.log1p(signal/background)-signal))
    return result

def compute_significance_asimov_uncertainty5pc(signal,background):
    result = 0
    if signal>0 and background>0:
        s = signal
        b = background
        sigmab = 0.05*b
        ln1 = math.log((s+b)*(b+sigmab**2.)/(b**2.+(s+b)*sigmab**2.))
        ln2 = math.log(1+sigmab**2.*s/(b*(b+sigmab**2.)))
        result = 0
        result = math.sqrt(2.*((s+b)*ln1 - b**2.*ln2/sigmab**2.))
    return result

def compute_significance_SoverSqrtB(signal,background):
    result = 0
    if signal>0 and background>0:
        result = signal/math.sqrt(background)
    return result

def compute_significance_SoverSqrtB_uncertainty5pc(signal,background):
    result = 0
    if signal>0 and background>0:
        result = signal/math.sqrt(background + (0.05*background)**2.)
    return result

def quad_sum(x):
    #print (x)
    mySumOfSquares = 0
    for ele in x:
       mySumOfSquares = mySumOfSquares + ele**2.
    return mySumOfSquares**0.5

def main(args=None):
    args = process_args(args)

    myAnalysis = Analysis(args.caliper_cfg)
    print(myAnalysis.figure_of_merit)
    #print ("myAnalysis.ignore = ",myAnalysis.ignore)

    list_files = []
    counter = 0
    for _, value in args._get_kwargs():
        if counter==0:
            counter = counter+1
            continue
        if value is not None:
            list_files = value[0].split(",")
    #print (list_files)

    if list_files:
        df_list = [pd.read_csv(file) for file in list_files]
        df = pd.concat(df_list)
        df.drop(["Unnamed: 0"],axis=1)
        df = df.reset_index()
        df[myAnalysis.figure_of_merit] = df[myAnalysis.figure_of_merit].astype('float') 
        df = df.groupby(list(myAnalysis.cut_on.keys()))[myAnalysis.figure_of_merit].apply(quad_sum).reset_index(name = myAnalysis.figure_of_merit)
        #print (df)
        print ("best set of cuts ("+myAnalysis.figure_of_merit+"):")
        for i,cut in enumerate(myAnalysis.cut_on):
            var_name = cut
            direction = myAnalysis.cut_on[cut]
            toPrint = direction
            this_cut = df.loc[df[myAnalysis.figure_of_merit].idxmax(),cut]
            if direction=="<":
                # get list of potential cuts
                list_cuts = df.loc[:,cut].tolist()
                list_cuts = list(dict.fromkeys(list_cuts))
                list_cuts = sorted(list_cuts)
                index_cut = list_cuts.index(df.loc[df[myAnalysis.figure_of_merit].idxmax(),cut])
                if index_cut+1 < len(list_cuts): this_cut = list_cuts[index_cut+1]
                else: this_cut = "+inf"
            if direction==">": direction = ">="
            print ("  on ",cut,direction,this_cut)
        print ("--> sensitivity is = ",df[myAnalysis.figure_of_merit].max())
        df.to_csv("out_combined.csv")
            #print (df.loc[df['asimov'].idxmax()])
        return
        
    df_sig = pd.read_csv(myAnalysis.df_sig)
    df_bkg = pd.read_csv(myAnalysis.df_bkg)

    df_sig = df_sig.drop(["dataset", "weight_nominal:sumw2", "n"], axis=1)
    df_bkg = df_bkg.drop(["dataset", "weight_nominal:sumw2", "n"], axis=1)

    for variable in list(myAnalysis.cut_on.keys()):
        df_sig = get_values_from_bin_ranges(df_sig, variable)
        df_bkg = get_values_from_bin_ranges(df_bkg, variable)

    if myAnalysis.type=="cut":
        df_sig2 = cumsum_nd2(df_sig, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        #df_sig2 = cumsum_nd(df_sig, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        #df_sig3 = cumsum_nd2(df_sig, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        df_sig2.reset_index(level=df_sig2.index.names, inplace=True)
        df_sig2 = df_sig2.rename(index=str, columns={myAnalysis.name_content: "signal"})

        df_bkg2 = cumsum_nd2(df_bkg, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        #df_bkg2 = cumsum_nd(df_bkg, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        df_bkg2.reset_index(level=df_bkg2.index.names, inplace=True)
        df_bkg2 = df_bkg2.rename(index=str, columns={myAnalysis.name_content: "background"})
        
        df_comb = df_sig2.merge(df_bkg2, on=list(myAnalysis.cut_on.keys()), how='left')
        if myAnalysis.figure_of_merit=="asimov":
            df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: compute_significance_asimov(x['signal'], x['background']), axis=1)
        elif myAnalysis.figure_of_merit=="asimov_uncertainty5pc":
            df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: compute_significance_asimov_uncertainty5pc(x['signal'], x['background']), axis=1)
        elif myAnalysis.figure_of_merit=="SoverSqrtB":
            df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: compute_significance_SoverSqrtB(x['signal'], x['background']), axis=1)
        elif myAnalysis.figure_of_merit=="SoverSqrtB_uncertainty5pc":
            df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: compute_significance_SoverSqrtB_uncertainty5pc(x['signal'], x['background']), axis=1)
        else:#fall back to asimov
            myAnalysis.figure_of_merit="asimov"
            df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: compute_significance_asimov(x['signal'], x['background']), axis=1)
        #df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: compute_significance(x['signal'], x['background'], myAnalysis.figure_of_merit), axis=1)
        df_comb["fractionS [%]"] = df_comb["signal"]/df_sig[myAnalysis.name_content].sum()*100.
        df_comb["fractionB [%]"] = df_comb["background"]/df_bkg[myAnalysis.name_content].sum()*100.
        del df_comb["index_x"]
        del df_comb["index_y"]

        print ("---------")
        print ("CUT OPTIMIZATION (will only use sensitivity of event passing cuts)")
        print ("---------")
        print ("best row:")
        print (df_comb.loc[[df_comb[myAnalysis.figure_of_merit].idxmax()]].to_string())
        print ("---------")
        print ("best set of cuts ("+myAnalysis.figure_of_merit+"):")
        for i,cut in enumerate(myAnalysis.cut_on):
            var_name = cut
            direction = myAnalysis.cut_on[cut]
            toPrint = direction
            this_cut = df_comb.loc[df_comb[myAnalysis.figure_of_merit].idxmax(),cut]
            if direction=="<":
                # get list of potential cuts
                list_cuts = df_comb.loc[:,cut].tolist()
                list_cuts = list(dict.fromkeys(list_cuts))
                list_cuts = sorted(list_cuts)
                index_cut = list_cuts.index(df_comb.loc[df_comb[myAnalysis.figure_of_merit].idxmax(),cut])
                if index_cut+1 < len(list_cuts): this_cut = list_cuts[index_cut+1]
                else: this_cut = "+inf"
            if direction==">": direction = ">="
            print ("  on ",cut,direction,this_cut)
        df_comb.to_csv(myAnalysis.out_df_name)

        #print (df_comb)

    else:
        df_sig2 = cumsum_nd2(df_sig, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        #df_sig2 = cumsum_nd(df_sig, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        df_sig2.reset_index(level=df_sig2.index.names, inplace=True)
        df_sig2 = df_sig2.rename(index=str, columns={myAnalysis.name_content: "signal_dir1"})
        df_sig2["signal_dir2"] = df_sig[myAnalysis.name_content].sum(axis=0) - df_sig2["signal_dir1"]

        df_bkg2 = cumsum_nd2(df_bkg, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        #df_bkg2 = cumsum_nd(df_bkg, myAnalysis.cut_on, myAnalysis.ignore, myAnalysis.name_content, myAnalysis.type)
        df_bkg2.reset_index(level=df_bkg2.index.names, inplace=True)
        df_bkg2 = df_bkg2.rename(index=str, columns={myAnalysis.name_content: "background_dir1"})
        df_bkg2["background_dir2"] = df_bkg[myAnalysis.name_content].sum(axis=0) - df_bkg2["background_dir1"]


        df_comb = df_sig2.merge(df_bkg2, on=list(myAnalysis.cut_on.keys()), how='left')

        if myAnalysis.figure_of_merit=="asimov":
            df_comb[myAnalysis.figure_of_merit+"_dir1"] = df_comb.apply(lambda x: compute_significance_asimov(x['signal_dir1'], x['background_dir1']), axis=1)
            df_comb[myAnalysis.figure_of_merit+"_dir2"] = df_comb.apply(lambda x: compute_significance_asimov(x['signal_dir2'], x['background_dir2']), axis=1)
        elif myAnalysis.figure_of_merit=="asimov_uncertainty5pc":
            df_comb[myAnalysis.figure_of_merit+"_dir1"] = df_comb.apply(lambda x: compute_significance_asimov_uncertainty5pc(x['signal_dir1'], x['background_dir1']), axis=1)
            df_comb[myAnalysis.figure_of_merit+"_dir2"] = df_comb.apply(lambda x: compute_significance_asimov_uncertainty5pc(x['signal_dir2'], x['background_dir2']), axis=1)
        elif myAnalysis.figure_of_merit=="SoverSqrtB":
            df_comb[myAnalysis.figure_of_merit+"_dir1"] = df_comb.apply(lambda x: compute_significance_SoverSqrtB(x['signal_dir1'], x['background_dir1']), axis=1)
            df_comb[myAnalysis.figure_of_merit+"_dir2"] = df_comb.apply(lambda x: compute_significance_SoverSqrtB(x['signal_dir2'], x['background_dir2']), axis=1)
        elif myAnalysis.figure_of_merit=="SoverSqrtB_uncertainty5pc":
            df_comb[myAnalysis.figure_of_merit+"_dir1"] = df_comb.apply(lambda x: compute_significance_SoverSqrtB_uncertainty5pc(x['signal_dir1'], x['background_dir1']), axis=1)
            df_comb[myAnalysis.figure_of_merit+"_dir2"] = df_comb.apply(lambda x: compute_significance_SoverSqrtB_uncertainty5pc(x['signal_dir2'], x['background_dir2']), axis=1)
        else:#fall back to asimov
            myAnalysis.figure_of_merit="asimov"
            df_comb[myAnalysis.figure_of_merit+"_dir1"] = df_comb.apply(lambda x: compute_significance_asimov(x['signal_dir1'], x['background_dir1']), axis=1)
            df_comb[myAnalysis.figure_of_merit+"_dir2"] = df_comb.apply(lambda x: compute_significance_asimov(x['signal_dir2'], x['background_dir2']), axis=1)
        #df_comb["asimov_dir1"] = df_comb.apply(lambda x: compute_significance(x['signal_dir1'], x['background_dir1'], myAnalysis.figure_of_merit), axis=1)
        #df_comb["asimov_dir2"] = df_comb.apply(lambda x: compute_significance(x['signal_dir2'], x['background_dir2'], myAnalysis.figure_of_merit), axis=1)
        df_comb[myAnalysis.figure_of_merit] = df_comb.apply(lambda x: math.sqrt(x[myAnalysis.figure_of_merit + '_dir1']**2. + x[myAnalysis.figure_of_merit + '_dir2']**2.), axis=1)
        df_comb["fractionS_dir1 [%]"] = df_comb["signal_dir1"]/df_sig[myAnalysis.name_content].sum()*100.
        df_comb["fractionB_dir1 [%]"] = df_comb["background_dir1"]/df_bkg[myAnalysis.name_content].sum()*100.
        df_comb["fractionS_dir2 [%]"] = df_comb["signal_dir2"]/df_sig[myAnalysis.name_content].sum()*100.
        df_comb["fractionB_dir2 [%]"] = df_comb["background_dir2"]/df_bkg[myAnalysis.name_content].sum()*100.

        del df_comb["index_x"]
        del df_comb["index_y"]

        print ("---------")
        print ("CATEGORY OPTIMIZATION (will quadratically combine sensitivities of two categories of events: the ones passing the cut and the ones failing)")
        print ("---------")
        print ("best row:")
        print (df_comb.loc[[df_comb[myAnalysis.figure_of_merit].idxmax()]].to_string())
        print ("---------")
        print ("best set of cuts ("+myAnalysis.figure_of_merit+"):")
        for i,cut in enumerate(myAnalysis.cut_on):
            var_name = cut
            direction = myAnalysis.cut_on[cut]
            toPrint = direction
            this_cut = df_comb.loc[df_comb[myAnalysis.figure_of_merit].idxmax(),cut]
            if direction=="<":
                # get list of potential cuts
                list_cuts = df_comb.loc[:,cut].tolist()
                list_cuts = list(dict.fromkeys(list_cuts))
                list_cuts = sorted(list_cuts)
                index_cut = list_cuts.index(df_comb.loc[df_comb[myAnalysis.figure_of_merit].idxmax(),cut])
                if index_cut+1 < len(list_cuts): this_cut = list_cuts[index_cut+1]
                else: this_cut = "+inf"
            if direction==">": direction = ">="
            print ("  on ",cut,direction,this_cut)
        df_comb.to_csv(myAnalysis.out_df_name)

    return 1


if __name__ == "__main__":
    main()
