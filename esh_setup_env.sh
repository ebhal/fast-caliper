# Run setup for cms-chip
CondaVer=3
MINICONDA_DIR=$HOME/miniconda${CondaVer}
CHIP_SOFTWARE=/home/hep/ebhal/chip_software/analysis
CHIP_OUTPUT=/home/hep/ebhal/chip_output

source $MINICONDA_DIR/etc/profile.d/conda.sh
conda activate chip_env
cd $CHIP_SOFTWARE
export PYTHONNOUSERSITE=true
export PYTHONPATH="$PWD:$PYTHONPATH"

cd ../fast_caliper
export PYTHONPATH="$PWD/fast_caliper:$PYTHONPATH"
#source /vols/grid/cms/setup.sh
#export X509_USER_PROXY=/home/hep/${USER}/.x509up_u$UID
