#!/bin/bash

function join_by { local IFS="$1"; shift; echo "$*"; }

process="$1"

if [[ "$process" == "ttH" ]]; then
    categories="1."
elif [[ "$process" == "VH" ]]; then
    categories="2."
elif [[ "$process" == "ggH" ]]; then
    categories="3."
fi

significance="$2"

configs=$(ls config/angular_var_category_${categories}*_${significance}.yaml)
for cfg in $configs; do
    fast_caliper $cfg
    final_cfg="$cfg"
done

out_files=$(ls output_dfs/out_category_${categories}*_${significance}.csv)
joined_files=$(join_by , $out_files)

fast_caliper -c $joined_files $final_cfg

mv out_combined.csv out_${process}_combined_${significance}.csv
