from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os

from fast_plotter.plotting import add_annotations
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml

plt.rcParams["figure.figsize"] = [5, 6]
plt.rcParams["axes.formatter.use_mathtext"] = True


def annotate_axes(axes, year, x_axis, process):
    # Sort out annotations from plotting config (with category and region replacements)
    if year == 2019:
        cfg = yaml.safe_load(open(os.path.join("..", "analysis", "plotting_configs", "plot_config_all_years_prescaled.yml"), "r"))
    else:
        cfg = yaml.safe_load(open(os.path.join("..", "analysis", "plotting_configs", "plot_config_{}.yml".format(year)), "r"))

    for i, line in enumerate(cfg["annotations"]):
        if r"${category}" in line["text"] or r"${region}" in line["text"]:
            line["text"] = line["text"].replace(r"${category}", process).replace(r"${region}", "signal region")
            cfg["annotations"][i] = line
    add_annotations(cfg["annotations"], axes)

    # Get x-axis label from plotting config, if possible
    var_replacements = cfg["bin_variable_replacements"]
    if x_axis in var_replacements.keys():
        x_label = var_replacements[x_axis]
    else:
        x_label = x_axis
    axes.set_xlabel(x_label)


def main(in_files, year, process="ttH", extension="pdf", analysis_cut=0.):
    ax = plt.gca()

    possible_signifs = {
        "SoverSqrtB": r"$S/\sqrt{B}$",
        "SoverSqrtB_uncertainty5pc": r"$S/\sqrt{B}$ (5% bkg. syst.)",
        "SoverSqrtB_uncertainty10pc": r"$S/\sqrt{B}$ (10% bkg. syst.)",
        "SoverSqrtB_uncertainty15pc": r"$S/\sqrt{B}$ (15% bkg. syst.)",
        "asimov": "Asimov",
        "asimov_uncertainty5pc": "Asimov (5% bkg. syst.)",
        "asimov_uncertainty10pc": "Asimov (10% bkg. syst.)",
        "asimov_uncertainty15pc": "Asimov (15% bkg. syst.)",
    }

    # Add dataframe info to these lists to make sure the same significances and variables are used for all dfs
    signifs = []
    x_axes = []
    dfs = []

    for f in in_files:
        df = pd.read_csv(f)
        if "Unnamed: 0" in df.columns:
            df.drop(columns=["Unnamed: 0"], inplace=True)

        x_axis = df.columns[0]
        x_axes.append(x_axis)
        df = df[np.isfinite(df[x_axis])]  # drop infinities
        df.reset_index(inplace=True)
        dfs.append(df)

        # Get the column name that contains the significance
        signif = []
        for col in possible_signifs.keys():
            if col in df.columns:
                signif.append(col)
        if len(signif) > 1:
            print("Multiple significance columns found: {}. Will use {}".format(signif, signif[0]))
        signif = signif[0]
        signifs.append(signif)

    if len(list(set(x_axes))) > 1:
        raise ValueError("Trying to aggregate different variables. Make sure each dataframe uses the same variable")
    x_axis = x_axes[0]

    # Make scatter plot with connecting line for each dataframe
    for i, df in enumerate(dfs):
        df.plot.scatter(
            x=x_axes[i],
            y=signifs[i],
            ax=ax,
            color=plt.cm.tab10(i),
            label=possible_signifs[signifs[i]]
        )
        df.plot.line(
            x=x_axes[i],
            y=signifs[i],
            label="",
            ax=ax,
            color=plt.cm.tab10(i)
        )

    if analysis_cut != 0.:
        ax.axvline(
            x=analysis_cut,
            linestyle='dashed',
            color='black'
        )

    ax.grid(True)
    ax.set_axisbelow(True)
    annotate_axes(ax, year, x_axis, process)

    ax.set_xlim([dfs[0][x_axis].min(), dfs[0][x_axis].max()])  # manually set x-axis limits from dataframes
    ax.set_ylim([0., ax.get_ylim()[1]])  # set lower y-axis limit to zero
    ax.set_ylabel("Significance")
    ax.legend(loc="upper right", prop={'size': 8})

    if len(set(signifs + list(possible_signifs.keys()))) == len(possible_signifs):
        signifs_for_filename = "all"
    else:
        signifs_for_filename = "_".join(signifs)
    out_file = "significance_{proc}_{x_ax}_{signifs}.{ext}".format(
        proc=process,
        x_ax=x_axis,
        signifs=signifs_for_filename,
        ext=extension
        )
    plt.savefig(out_file, dpi=200, bbox_inches='tight')
    print("Saving output file:", out_file)


if __name__ == "__main__":
    parser = ArgumentParser(description=__doc__, formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("dataframes", type=str, nargs="+", help="Dataframe with significances")
    parser.add_argument("--year", "-y", type=int, required=True, choices=[2016, 2017, 2018, 2019], help="Year of data taking simulated. Only required for retrieving plotting annotations. 2019 retrieves the full Run-2 lumi-prescaled config")
    parser.add_argument("--process", "-p", type=str, default="ttH", help="Signal process")
    parser.add_argument("--extension", "-e", type=str, default="pdf", help="Extension for output plot")
    parser.add_argument("--analysis_cut", "-a", type=float, default=0., help="Value of cut placed in analysis for a dashed line indicator. Default value will not plot a line")
    args = parser.parse_args()

    main(args.dataframes, args.year, process=args.process, extension=args.extension, analysis_cut=args.analysis_cut)
